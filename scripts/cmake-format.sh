#!/bin/bash
if [ $# -ne 1 ]; then
    echo "Illegal number of arguments"
    echo "Usage: cmake-format.sh <project_dir>"
    exit 1
fi

cmake-format $1/CMakeLists.txt -o $1/CMakeLists.txt
exit $?
