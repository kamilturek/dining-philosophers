#pragma once

enum class State { HUNGRY, EATING, THINKING };