#pragma once
#include <mutex>

class Fork
{
public:
    explicit Fork(int id);

    std::mutex mutex;

    int getId() const;

private:
    int id;
};
