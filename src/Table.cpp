#include "Table.hpp"

Table::Table() : forks({ Fork(0), Fork(1), Fork(2), Fork(3), Fork(4) })
{
}

void Table::startDinner()
{
    dining = true;
}

void Table::finishDinner()
{
    dining = false;
}

bool Table::isDining() const
{
    return dining;
}