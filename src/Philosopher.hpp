#pragma once
#include "Fork.hpp"
#include "Random.hpp"
#include "State.hpp"
#include "Table.hpp"
#include <chrono>
#include <iostream>
#include <memory>
#include <string>
#include <thread>

class Philosopher
{
public:
    Philosopher(const std::string& name, const Table& table, Fork& leftFork, Fork& rightFork);
    ~Philosopher();

    float getProgress() const;
    State getState() const;
    std::string getName() const;
    std::string getStateString() const;
    std::pair<int, int> getForkIds() const; 

private:
    const std::string name;
    const Table& table;
    Fork& leftFork;
    Fork& rightFork;
    std::atomic<float> progress = 0.0f;
    std::atomic<State> state = State::THINKING;
    std::thread thread;

    void dine();
    void think();
    void eat();
    void wait();
};
