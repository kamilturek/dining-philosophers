#include "Square.hpp"

Square::Square(int size, int rowIndex, int colIndex) : width(size), height(2 * size - 1)
{
    window = newwin(width, height, rowIndex, colIndex);
    box(window, 0, 0);
    wrefresh(window);
}

Square::~Square()
{
    wborder(window, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
    wrefresh(window);
    delwin(window);
    endwin();
}

void Square::setReversed(bool value)
{
    if (reversed == value)
        return;

    reversed = value;

    if (value)
        wattron(window, A_REVERSE);

    for (int i = 1; i < width - 1; i++)
        for (int j = 1; j < height - 1; j++)
            mvwprintw(window, i, j, " ");
            
    wattroff(window, A_REVERSE);
    wrefresh(window);
}

bool Square::isReversed()
{
    return reversed;
}