#include "UI.hpp"
#include <iostream>

UI::UI(Table& table, const std::array<Philosopher, 5>& philosophers) : table(table), philosophers(philosophers)
{
    initscr();
    keypad(stdscr, TRUE);
    curs_set(0);
    cbreak();
    noecho();

    checkWindowSize();

    initializeMainWindow();
    initializeStatusWindow();
    initializeVisualizationWindow();
    initializeHelpWindow();
    initializePhilosopherStatuses();
    initializeProgressBars();
    initializeSquares();

    viewThread = std::make_unique<std::thread>(&UI::refreshView, this);
    keyboardThread = std::make_unique<std::thread>(&UI::waitForPressedKey, this);
}

UI::~UI()
{
    keyboardThread->join();
    viewThread->join();

    destroyProgressBars();
    destroyWindow(helpWindow);
    destroyWindow(visualizationWindow);
    destroyWindow(statusWindow);
    destroyWindow(mainWindow);
    endwin();
}

bool UI::isReady() const
{
    return ready;
}

void UI::checkWindowSize()
{
    if (LINES >= 35 && COLS >= 95)
    {
        ready = true;
        return;
    }

    const std::string warningLine1 = "IT IS RECOMMENDED TO RUN THIS APPLICATION IN TERMINAL WINDOW";
    const std::string warningLine2 = "OF AT LEAST 35 LINES HEIGHT AND 95 COLUMNS WIDTH";
    const std::string warningLine3 = "PRESS [ENTER] TO PROCEED";
    
    const int rowIndex = LINES / 2;
    mvprintw(rowIndex, (COLS - warningLine1.length()) / 2, warningLine1.c_str());
    mvprintw(rowIndex + 1, (COLS - warningLine2.length()) / 2, warningLine2.c_str());
    mvprintw(rowIndex + 2, (COLS - warningLine3.length()) / 2, warningLine3.c_str());

    while (getch() != 10);
    ready = true;
}

void UI::initializeMainWindow()
{
    mainWindow = newwin(LINES, COLS, 0, 0);
    box(mainWindow, 0, 0);

    const std::string leftHeader = "DINING PHILOSOPHERS PROBLEM | SO2";
    const std::string rightHeader = "KAMIL TUREK 2020";

    mvwprintw(mainWindow, 0, 5, leftHeader.c_str());
    mvwprintw(mainWindow, 0, COLS - rightHeader.length() - 5, rightHeader.c_str());
    wrefresh(mainWindow);
}

void UI::initializeStatusWindow()
{
    const int windowHeight = LINES - 3;
    constexpr int windowWidth = 28;

    statusWindow = newwin(windowHeight, windowWidth, 2, 1);
    box(statusWindow, 0, 0);
    mvwprintw(statusWindow, 0, 4, "PHILOSOPHER STATUS");

    const int rowGap = windowHeight / philosophers.size();
    constexpr int colIndex = 5;

    for (size_t i = 0; i < philosophers.size(); i++)
    {
        const int rowIndex = rowGap * i + 2;
        outputCoords[i] = { { rowIndex, colIndex }, { rowIndex + 1, colIndex }, { rowIndex + 4, colIndex } };
    }

    wrefresh(statusWindow);
}

void UI::initializeVisualizationWindow()
{
    const int windowHeight = LINES - 8;
    const int windowWidth = COLS - 30;

    visualizationWindow = newwin(windowHeight, windowWidth, 2, 29);
    box(visualizationWindow, 0, 0);
    mvwprintw(visualizationWindow, 0, 5, "VISUALIZATION");
    wrefresh(visualizationWindow);
}

void UI::initializeSquares()
{
    const int windowHeight = LINES - 8;
    const int windowWidth = COLS - 30;

    const int philosopherSize = 5;
    const std::pair<int, int> baseCoords = { windowHeight / 3, windowWidth / 2 + 25 };
    const std::array<std::pair<int, int>, 5> philosopherCoords{
        {
            { baseCoords.first, baseCoords.second },
            { baseCoords.first + 5, baseCoords.second + 9 },
            { baseCoords.first + 13, baseCoords.second + 9 },
            { baseCoords.first + 13, baseCoords.second - 9 },
            { baseCoords.first + 5, baseCoords.second - 9 },
        }
    };

    const int forkSize = 3;
    const std::array<std::pair<int, int>, 5> forkCoords{
        {
            { baseCoords.first + 2, baseCoords.second - 5 },
            { baseCoords.first + 2, baseCoords.second + 9 },
            { baseCoords.first + 10, baseCoords.second + 9},
            { baseCoords.first + 13, baseCoords.second + 2},
            { baseCoords.first + 10, baseCoords.second - 5},
        }
    };

    for (size_t i = 0; i < 5; i++)
    {
        philosopherSquares.push_back(std::make_unique<NumberedSquare>(philosopherSize, philosopherCoords.at(i).first, philosopherCoords.at(i).second, i + 1));
        forkSquares.push_back(std::make_unique<Square>(forkSize, forkCoords.at(i).first, forkCoords.at(i).second));
    }
}

void UI::initializeHelpWindow()
{
    const int windowHeight = 5;
    const int windowWidth = COLS - 30;

    helpWindow = newwin(windowHeight, windowWidth, (LINES - 8) + 2, 29);
    box(helpWindow, 0, 0);
    mvwprintw(helpWindow, 0, 5, "HELP");
    mvwprintw(helpWindow, 1, 2, "STATUSES: THINKING -> HUNGRY -> ");
    wattron(helpWindow, A_REVERSE);
    wprintw(helpWindow, "EATING");
    wattroff(helpWindow, A_REVERSE);

    std::string escHint = "[ESC] - FINISH DINNER";
    mvwprintw(helpWindow, 1, windowWidth - escHint.length() - 2, escHint.c_str());

    wrefresh(helpWindow);
}

void UI::initializePhilosopherStatuses()
{
    for (size_t i = 0; i < philosophers.size(); i++)
    {
        std::pair<int, int> coords = outputCoords.at(i).name;
        auto name = philosophers.at(i).getName().c_str();
        mvwprintw(statusWindow, coords.first, coords.second, name);
    }
    wrefresh(statusWindow);
}

void UI::initializeProgressBars()
{    
    for (size_t i = 0; i < philosophers.size(); i++)
    {
        std::pair<int, int> coords = outputCoords.at(i).progressBar;
        WINDOW* bar = newwin(barHeight, barWidth, coords.first - 1, coords.second);
        progressBars[i] = bar;
        box(bar, 0, 0);
        wrefresh(bar);
    }
}

void UI::destroyWindow(WINDOW* window)
{
    wborder(window, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
    wrefresh(window);
    delwin(window);
}

void UI::destroyProgressBars()
{
    for (WINDOW* bar : progressBars)
        destroyWindow(bar);
}   

void UI::refreshView()
{
    while (table.isDining())
    {
        refreshStates();
        refreshProgressBars();
        refreshSquares();
        wrefresh(statusWindow);
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }
}

void UI::refreshStates()
{
    for (size_t i = 0; i < philosophers.size(); i++)
    {
        std::pair<int, int> coords = outputCoords.at(i).state;
        const auto& philosopher = philosophers.at(i);
        box(progressBars.at(i), 0, 0);
        mvwprintw(statusWindow, coords.first, coords.second, philosopher.getStateString().c_str());
    }
}

void UI::refreshProgressBars()
{
    for (size_t i = 0; i < philosophers.size(); i++)
    {
        WINDOW* bar = progressBars.at(i);

        wmove(bar, 1, 1);
        for (size_t j = 0; j < barWidth - 2; j++)
            wprintw(bar, " ");

        const int progress = philosophers.at(i).getProgress() * (barWidth - 1);

        wmove(bar, 1, 1);
        wattron(bar, A_REVERSE);
        for (int j = 0; j < progress; j++)
            wprintw(bar, " ");    
        wattroff(bar, A_REVERSE);
        wrefresh(bar);
    }
}

void UI::refreshSquares()
{
    std::array<bool, 5> forkStates;
    forkStates.fill(false);

    for (size_t i = 0; i < philosophers.size(); i++)
    {
        const auto& philosopher = philosophers.at(i);
        const auto forks = philosopher.getForkIds();
        
        if (philosopher.getState() == State::EATING)
        {
            philosopherSquares.at(i)->setReversed(true);
            forkStates.at(forks.first) = true;
            forkStates.at(forks.second) = true;
        }
        else
            philosopherSquares.at(i)->setReversed(false);
    }

    for (size_t i = 0; i < forkStates.size(); i++)
        forkSquares.at(i)->setReversed(forkStates[i]);
}

void UI::waitForPressedKey()
{
    while (table.isDining())
    {
        int keyPressed = wgetch(helpWindow);

        switch (keyPressed)
        {
            case 27: // ESCAPE KEY
                table.finishDinner();
                break;
        }
    }
}