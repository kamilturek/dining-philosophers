#pragma once
#include "Square.hpp"
#include <string>

class NumberedSquare : public Square
{
public:
    NumberedSquare(int size, int rowIndex, int colIndex, int number);

    virtual void setReversed(bool value) override;

private:
    const int number;

    void displayNumber(bool value);
};