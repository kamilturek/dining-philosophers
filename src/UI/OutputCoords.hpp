#pragma once
#include <utility>

struct OutputCoords
{
    std::pair<int, int> name;
    std::pair<int, int> state;
    std::pair<int, int> progressBar;
};