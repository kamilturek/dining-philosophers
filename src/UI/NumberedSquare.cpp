#include "NumberedSquare.hpp"

NumberedSquare::NumberedSquare(int size, int rowIndex, int colIndex, int number) : Square(size, rowIndex, colIndex), number(number)
{
    displayNumber(false);
}

void NumberedSquare::setReversed(bool value)
{
    Square::setReversed(value);
    displayNumber(value);
}

void NumberedSquare::displayNumber(bool value)
{
    if (value)
        wattron(window, A_REVERSE);
    
    mvwprintw(window, height / 2 - 2, width / 2 + 2, std::to_string(number).c_str());
    wrefresh(window);
    wattroff(window, A_REVERSE);
}