#pragma once
#include <ncurses.h>

class Square
{
public:
    Square(int size, int rowIndex, int colIndex);
    virtual ~Square();

    virtual void setReversed(bool value);
    bool isReversed();

protected:
    WINDOW* window;
    const int width;
    const int height;
    bool reversed = false;
};