#pragma once
#include <ncurses.h>
#include <string>
#include <array>
#include <thread>
#include "Philosopher.hpp"
#include "OutputCoords.hpp"
#include "Square.hpp"
#include "NumberedSquare.hpp"

class UI
{
public:
    UI(Table& table, const std::array<Philosopher, 5>& philosophers);
    ~UI();

    bool isReady() const;

private:
    static constexpr int barHeight = 3;
    static constexpr int barWidth = 20;

    bool ready = false;

    std::unique_ptr<std::thread> viewThread;
    std::unique_ptr<std::thread> keyboardThread;

    WINDOW* mainWindow;
    WINDOW* statusWindow;
    WINDOW* visualizationWindow;
    WINDOW* helpWindow;

    std::array<OutputCoords, 5> outputCoords; 
    std::array<WINDOW*, 5> progressBars;
    std::vector<std::unique_ptr<NumberedSquare>> philosopherSquares;
    std::vector<std::unique_ptr<Square>> forkSquares;

    Table& table; 
    const std::array<Philosopher, 5>& philosophers;

    void checkWindowSize();

    void initializeMainWindow();
    void initializeStatusWindow();
    void initializeVisualizationWindow();
    void initializeHelpWindow();
    void initializePhilosopherStatuses();
    void initializeProgressBars();
    void initializeSquares();

    void destroyWindow(WINDOW* window);
    void destroyProgressBars();

    void refreshView();
    void refreshStates();
    void refreshProgressBars();
    void refreshSquares();

    void waitForPressedKey();
};