#include "Philosopher.hpp"
#include "Table.hpp"
#include "UI.hpp"
#include <array>
#include <chrono>
#include <iostream>
#include <thread>

void run()
{
    std::this_thread::sleep_for(std::chrono::seconds(1));

    Table table;
    std::array<Philosopher, 5> philosophers{
        {
            { "PHILOSOPHER 1", table, table.forks.at(0), table.forks.at(1) },
            { "PHILOSOPHER 2", table, table.forks.at(1), table.forks.at(2) },
            { "PHILOSOPHER 3", table, table.forks.at(2), table.forks.at(3) },
            { "PHILOSOPHER 4", table, table.forks.at(3), table.forks.at(4) },
            { "PHILOSOPHER 5", table, table.forks.at(4), table.forks.at(0) },
        }
    };
    UI ui(table, philosophers);

    while (!ui.isReady());
    table.startDinner();
    while(table.isDining());
}

int main(int, const char**)
{
    run();

    return 0;
}
