#include "Philosopher.hpp"

Philosopher::Philosopher(const std::string& name, const Table& table, Fork& leftFork, Fork& rightFork)
    : name(name), table(table), leftFork(leftFork), rightFork(rightFork), thread(&Philosopher::dine, this)
{
}

Philosopher::~Philosopher()
{
    thread.join();
}

void Philosopher::dine()
{
    while (!table.isDining());
    while (table.isDining())
    {
        think();
        eat();
    }
}

void Philosopher::think()
{
    state = State::THINKING;
    wait();
    state = State::HUNGRY;
}

void Philosopher::eat()
{
    // std::scoped_lock (since C++17) (cppreference.com)
    // Takes an ownership of the mutexes using deadlock avoidance algorithm as std::lock
    // Mutexes are released in reverse order

    std::scoped_lock lock(leftFork.mutex, rightFork.mutex);
    state = State::EATING;
    wait();
}

void Philosopher::wait()
{
    int delayCount = Random().randomInt(15, 50);

    for (int i = 1; i <= delayCount; i++)
    {
        if (!table.isDining())
            break;

        std::this_thread::sleep_for(std::chrono::milliseconds(200));
        progress = static_cast<float>(i) / static_cast<float>(delayCount);
    }
    progress = 0.0f;
}

std::string Philosopher::getName() const
{
    return name;
}

std::string Philosopher::getStateString() const
{
    switch (state)
    {
        case State::HUNGRY:
            return "HUNGRY";
        case State::EATING:
            return "EATING";
        case State::THINKING:
            return "THINKING";
    }

    throw std::runtime_error("Undefined state");
}

std::pair<int, int> Philosopher::getForkIds() const
{
    return std::make_pair(leftFork.getId(), rightFork.getId());
}

State Philosopher::getState() const
{
    return state;
}

float Philosopher::getProgress() const
{
    return progress;
}