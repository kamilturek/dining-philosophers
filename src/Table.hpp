#pragma once
#include "Fork.hpp"
#include <array>
#include <atomic>
#include <vector>

class Table
{
public:
    Table();

    void startDinner();
    void finishDinner();
    bool isDining() const;

    std::array<Fork, 5> forks;
    
private:
    std::atomic<bool> dining = false;
};
